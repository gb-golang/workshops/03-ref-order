package main

import (
	"fmt"
)

func printOrder(quantity, price int, customerName, productName string) {
	total := quantity * price
	fmt.Printf("Заказ от %s: %dx %s (Итого: $%d)\n", customerName, quantity, productName, total)
}

func main() {
	printOrder(12, 300, "Иван", "Книга")
}
